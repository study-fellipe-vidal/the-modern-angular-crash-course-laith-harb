import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  newMemberName: string = ""
  members: string[] = []
  errorMessage: string = ""
  numberOfTeams: number | "" = ""
  teams: string[][] = []

  addMember() {

    if(!this.newMemberName) {
      this.errorMessage = "Name can't be empty"
      return
    }

    this.members.push(this.newMemberName)
    this.newMemberName = ""
    this.errorMessage = ""
  }

  onInput(member: string){
    this.newMemberName = member
  }

  onNumberOfTeamsInput(value: string){
    this.numberOfTeams = Number(value)
  }

  generateTeams(){

    if (!this.numberOfTeams || this.numberOfTeams <= 0) {
      this.errorMessage = "Invalid number of teams"
      return
    }

    if (this.members.length < this.numberOfTeams){
      this.errorMessage = "Not Enough members"
      return
    }

    this.teams = []

    const remainingMembers = [...this.members]

    while(remainingMembers.length){
      for(let i = 0; i < this.numberOfTeams; i++) {
        const randomIndex = Math.floor(Math.random() * remainingMembers.length)
        const member = remainingMembers.splice(randomIndex, 1)[0]


        // Break loop if member is undefined aka no members remaining
        if(!member) break

        /**
         * Push current member to array if the array exists
         * or create a new array with member
         */
        if(this.teams[i]){
          this.teams[i].push(member)
        }
        else {
          this.teams[i] = [member]
        }
      }
    }

    // Clean up
    this.members = []
    this.numberOfTeams = ""
    this.errorMessage = ""

  }
}
